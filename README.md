# MSUTinyTitan

This is our implementation of [TinyTitan](https://tinytitan.github.io/), a educational "supercomputer" for use at outreach events.
This does not do anything fancy, other than combating the Raspberry Pi shortage of the early 2020s by enlisting the help of [Le Potato](https://libre.computer/products/aml-s905x-cc/), a Pi clone that does most of what I need it to do.

## Parts List

- 1 storage container for a stack of Raspberry Pi's. I chose [this one](https://www.amazon.com/dp/B083FDHPBH), since I'm a sucker for RGB.
- A 4 pack of [Le Potato](https://www.amazon.com/dp/B0BDR2LWPC)
- Ideally one Raspberry Pi 3+
- Assorted microSD cards (you'll need 4)
- A network switch, the smaller the better. 4 ports is all you'll need.
- [Microusb cables](https://www.amazon.com/dp/B095JZSHXQ)
- Networking cables, cut to length.

## Setup

The [TinyTitan SPH code](https://github.com/TinyTitan/SPH) was originally written for very old Raspberry Pi's.
As such, it in principle will run on anything, but at least the graphics are easiest to get running on bonafide Raspberry Pi hardware.
The simple reason for this is two-fold: the original makefile links against some of the closed source Broadcom libraries that are specific to Raspberry Pi's.
These are almost all graphics libraries, that aren't needed for running the spherical particle hydrodynamics simulations that we are keen to do.
Now, on more modern/more powerful hardware (like a Jetson nano or a Pi 4 clone), it would be possible to use the glfw codepath and be very happy... However, the potatoes I chose are Raspberry Pi 3+ clones, and thus only have OpenGL 2.0 support, which isn't high enough for glfw to render successfully on the GPU cores, and you'd need to fall back to straight CPU rendering that is not performant.
To make matters worse, I'm bad at writing OpenGL code, so I didn't want to change what was in here.
This is bad.

What I have done is made a hybrid cluster, using 1 pre-pandemic Raspberry Pi 3+ that I had laying around, and compiling SPH with rendering and graphics on that hardware.
Then, I made a second copy of the SPH code that is meant to only run on the potatoes, stripping out all the graphics dependencies, so that the potatos run the compute processes.
These two binaries could then be run together with OpenMPI across a small Pi cluster that is portable to demos in various locations.
For interested parties, images of the SD cards used are provided in this repository, one for the main Raspberry Pi that runs graphics (and can in principle act as the internet gateway if you let it), and one for the potatoes.

## Other notes

The images are built on top of [Raspbian buster](https://downloads.raspberrypi.org/raspios_oldstable_armhf/images/raspios_oldstable_armhf-2023-02-22/2023-02-21-raspios-buster-armhf.img.xz), as that is the newest version of Raspbian that still has the old `/opt/vc` graphics stack that the Raspberry Pi build needed for accelerated graphics with the SPH code we are using.
For the potatoes, there is a similar image available for [buster](https://distro.libre.computer/ci/raspbian/10/2022-09-22-raspbian-buster-armhf%2Baml-s905x-cc.img.xz).
In both cases, there are minimal packages that need to be downloaded.
```bash
sudo apt install libglew-dev
sudo apt install openmpi-bin
```
I made the switch here to `OpenMPI`, rather than `mpich`, which was more tolerant of my unique networking setup.
During the compilation, you may find some minor annoyances if you do this from scratch.
You end up needing to make symlinks to shared object files inside of `/opt/vc`, as some of the graphics libraries are renamed in buster compared with older Raspbian builds that the original TinyTitan builders used.
But otherwise, if you've compiled scientific software before, there is nothing too out of the ordinary.

Networking is another point of contention.
What ended up working the best for me is setting up static IPs on the potatos and the pi (192.168.12.X), with the Pi set up to connect to WiFi networks and share that connectivity.
There are multiple guides and forum posts set up to do this on the net, but just using the images and adjusting the contents of `/etc/dhcpcd.conf` and `/etc/dnsmasq.conf` from the images I provide is probably easiest.